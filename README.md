# Docker Images

![Docker](img/docker.png)

**Docker** provides the ability to package and run an application in a loosely
isolated environment called a container. The isolation and security allows you
to run many containers simultaneously on a given host. Containers are
lightweight and contain everything needed to run the application, so you do not
need to rely on what is currently installed on the host.

# Folder Structure

All of the images to build live under the **images** folder. Adding a new image
to build involves creating a new folder under images with a **Dockerfile** and
**VERSION** file. The pipeline uses templates for reusability when building the
images.

Refer to [gitlab folder](gitlab/) for more examples on how to add a
new image to build.

```yml
include:
  - local: gitlab/templates/docker.yml

.bandit:
  variables:
    BUILD_DIR: ${CI_PROJECT_DIR}/images/bandit
    IMAGE_NAME: ${CI_REGISTRY_IMAGE}/bandit
    IMAGE_ARG: BANDIT_VERSION

lint-bandit:
  stage: lint
  extends:
    - .bandit
    - .lint_image
  only:
    changes:
      - images/bandit/*
```

# Bandit

**Bandit** is a tool designed to find common security issues in Python code. To
do this Bandit processes each file, builds an AST from it, and runs appropriate
plugins against the AST nodes. Once Bandit has finished scanning all the files
it generates a report.

Refer to [Bandit](https://github.com/PyCQA/bandit) for documentation.

```yml
variables:
  PIP_CACHE_DIR: $CI_PROJECT_DIR/.cache/pip

cache:
  paths:
    - $PIP_CACHE_DIR

.pip_install:
  before_script:
    - mkdir --parents $PIP_CACHE_DIR
    - pip
        --cache-dir=$PIP_CACHE_DIR
        install --requirement ${BUILD_DIR}/requirements.txt

scan_python:
  image:
    name: registry.gitlab.com/massimocannavo/docker-images/bandit:1.7.4
    entrypoint: [""]
  extends:
    - .pip_install
  script:
    - bandit --configfile bandit.yml --recursive $BUILD_DIR
```

# Cppcheck

Cppcheck is a static analysis tool for C/C++ code. It provides unique code
analysis to detect bugs and focuses on detecting undefined behaviour and
dangerous coding constructs. The goal is to have very few false positives.
Cppcheck is designed to be able to analyze your C/C++ code even if it has
non-standard syntax (common in embedded projects).

Refer to [cppcheck](https://cppcheck.sourceforge.io) for documentation.

```yml
variables:
  SOURCE_DIR: ${CI_PROJECT_DIR}/src

lint:
  stage: lint
  image:
    name: registry.gitlab.com/massimocannavo/docker-images/cppcheck:2.8
    entrypoint: [""]
  script:
    - cppcheck
        --enable=all
        --inconclusive
        --error-exitcode=1
        --suppress=missingIncludeSystem
        $SOURCE_DIR
```

# Flawfinder

Flawfinder is a simple program that scans C/C++ source code and reports
potential security flaws. It can be a useful tool for examining software for
vulnerabilities, and it can also serve as a simple introduction to static source
code analysis tools more generally. It is designed to be easy to install and
use. Flawfinder supports the Common Weakness Enumeration (CWE) and is officially
CWE-Compatible.

Refer to [flawfinder](https://github.com/david-a-wheeler/flawfinder) for documentation.

```yml
variables:
  SOURCE_DIR: ${CI_PROJECT_DIR}/src
  TESTS_DIR: ${CI_PROJECT_DIR}/tests

scan:
  stage: scan
  image:
    name: registry.gitlab.com/massimocannavo/docker-images/flawfinder:2.0.19
    entrypoint: [""]
  script:
    - flawfinder --error-level 1 $SOURCE_DIR
    - flawfinder --minlevel 3 --error-level 3 $TESTS_DIR
```

# GCC

The GNU Compiler Collection includes front ends for C, C++, Objective-C,
Fortran, Ada, Go, and D, as well as libraries for these languages
(libstdc++,...). GCC was originally written as the compiler for the GNU
operating system. The GNU system was developed to be 100% free software, free in
the sense that it respects the user's freedom.

Refer to [gcc](https://gcc.gnu.org/) for documentation.

```yml
variables:
  SOURCE_DIR: ${CI_PROJECT_DIR}/src
  TESTS_DIR: ${CI_PROJECT_DIR}/tests

test:
  stage: test
  image:
    name: registry.gitlab.com/massimocannavo/docker-images/gcc:11.2.0
    entrypoint: [""]
  script:
    - make -f Makefile.test
    - cd $TESTS_DIR
    - ./run_test
  artifacts:
    paths:
      - $SOURCE_DIR
```

# jsonschema

**jsonschema** is an implementation of the JSON Schema specification for Python.
JSON Schema is a powerful tool for validating the structure of JSON data.

Refer to [jsonschema](https://github.com/python-jsonschema/jsonschema) for
documentation.

```yml
validate-json:
  stage: lint
  image:
    name: registry.gitlab.com/massimocannavo/docker-images/jsonschema:4.16.0
    entrypoint: [""]
  script:
    - cd $TF_ROOT
    - python /home/jsonschema/json_schema.py --path schema
```

# Pylint

**Pylint** analyses your code without actually running it. It checks for errors,
enforces a coding standard, looks for code smells, and can make suggestions
about how the code could be refactored.

Refer to [pylint](https://github.com/PyCQA/pylint) for documentation.

```yml
variables:
  PIP_CACHE_DIR: $CI_PROJECT_DIR/.cache/pip

cache:
  paths:
    - $PIP_CACHE_DIR

.pip_install:
  before_script:
    - mkdir --parents $PIP_CACHE_DIR
    - pip
        --cache-dir=$PIP_CACHE_DIR
        install --requirement ${BUILD_DIR}/requirements.txt

.lint_python:
  image:
    name: registry.gitlab.com/massimocannavo/docker-images/pylint:2.15.2
    entrypoint: [""]
  extends:
    - .pip_install
  script:
    - export PYTHONPATH=${BUILD_DIR}/bin
    - pylint --output-format=colorized --recursive yes $BUILD_DIR
```

# Pytest

The **pytest** framework makes it easy to write small, readable tests, and can
scale to support complex functional testing for applications and libraries.

Refer to [pytest](https://github.com/pytest-dev/pytest) for documentation.

```yml
variables:
  PIP_CACHE_DIR: $CI_PROJECT_DIR/.cache/pip

cache:
  paths:
    - $PIP_CACHE_DIR

.pip_install:
  before_script:
    - mkdir --parents $PIP_CACHE_DIR
    - pip
        --cache-dir=$PIP_CACHE_DIR
        install --requirement ${BUILD_DIR}/requirements.txt

.test_python:
  image:
    name: registry.gitlab.com/massimocannavo/docker-images/pytest:7.1.3
    entrypoint: [""]
  extends:
    - .pip_install
  script:
    - coverage run --module pytest
    - coverage report --show-missing
```
