"""Validates a json file against a schema."""
from argparse import ArgumentParser, Namespace
import json
import logging
from os import environ, path, walk
import sys

from jsonschema import validate
from jsonschema.exceptions import ValidationError

DEFAULT_LOG_LEVEL = "INFO"
DEFAULT_LOG_FORMAT = "[%(asctime)s] [%(levelname)8s] %(message)s"


def main() -> None:
    """Main entry point of the script."""
    success = True
    args = parse_args()
    log_level = logging.getLevelName(args.log_level.upper())
    logging.basicConfig(level=log_level, format=args.log_format)

    json_files = get_files(folder_path=args.path)
    json_schemas = [k for k in json_files if "schema" in k]
    for schema in json_schemas:
        json_file = json_files.get(schema.removesuffix("-schema"))
        schema_data = read_json(json_files.get(schema))
        json_data = read_json(json_file)
        if not all((schema_data, json_data)):
            sys.exit(1)

        file_name = path.basename(json_file)
        try:
            validate(json_data, schema_data)
            logging.info(" ".join([file_name.ljust(40), "OK"]))
        except ValidationError as ex:
            logging.error(ex.message)
            logging.info(" ".join([file_name.ljust(40), "FAILED"]))
            success = False

    if not success:
        sys.exit(1)


def get_files(folder_path: str) -> dict:
    """
    Returns a dictionary of json files found.

    Parameters
    ==========
    folder_path : str
        Path of the folder to look for the files.
    """
    files_map = {}
    if not path.isdir(folder_path):
        logging.error("path %s does not exist", folder_path)
        return files_map

    for root, _, files in walk(folder_path):
        for file in files:
            if file.endswith(".json"):
                files_map[file.removesuffix(".json")] = f"{root}/{file}"

    return files_map


def parse_args() -> Namespace:
    """Parses arguments specified from command line."""
    parser = ArgumentParser()
    parser.add_argument(
        "--log-level",
        default=environ.get("LOG_LEVEL", DEFAULT_LOG_LEVEL),
        help="Level of logging to use in the script.",
        type=str,
    )
    parser.add_argument(
        "--log-format",
        default=environ.get("LOG_FORMAT", DEFAULT_LOG_FORMAT),
        help="Format of the log message to use in the script.",
        type=str,
    )

    required = parser.add_argument_group("required arguments")
    required.add_argument(
        "--path",
        help="Path to the json files to validate.",
        required=True,
        type=str,
    )

    return parser.parse_args()


def read_json(file_path: str) -> dict:
    """Reads a json file into a dictionary."""
    json_data = {}
    try:
        with open(file_path, encoding="utf-8", mode="r") as file:
            json_data = json.load(file)
    except FileNotFoundError:
        logging.error("file [%s] not found", file_path)
    except json.JSONDecodeError as exc:
        logging.error("invalid json, unable to decode - %s", exc)

    return json_data


if __name__ == "__main__":
    main()
