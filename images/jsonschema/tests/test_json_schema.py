"""Unit test for json_schema script."""
import argparse
import json
from os import path
from os.path import dirname, realpath
from unittest.mock import patch

import pytest

from json_schema import get_files, main, parse_args, read_json

MODULE = main.__module__


class TestJsonSchema:
    """Unit test class for json-schema script."""

    @classmethod
    def setup_class(cls):
        """Setup function used by the unit tests."""
        cls.data_dir = f"{dirname(realpath(__file__))}/data"
        cls.data_files = {
            "schema": f"{cls.data_dir}/movies-schema.json",
            "movies": f"{cls.data_dir}/movies.json",
            "failed": f"{cls.data_dir}/movies-invalid.json",
            "invalid": f"{cls.data_dir}/invalid.json",
        }
        arguments = {
            "log_level": "INFO",
            "log_format": "[%(asctime)s] [%(levelname)8s] %(message)s",
            "path": cls.data_dir,
        }
        cls.args = argparse.Namespace(**arguments)

    def test_get_files_invalid_path(self, caplog):
        """Test invalid path was specified."""
        folder_path = "folder"
        log_mesg = f"path {folder_path} does not exist"

        with caplog.at_level(self.args.log_level, logger="root"):
            assert not get_files(folder_path)
            assert log_mesg in caplog.text

    def test_get_files(self):
        """Test handling json files found."""
        files_map = {}
        for file in self.data_files.values():
            file_name = path.basename(file).removesuffix(".json")
            files_map[file_name] = file

        assert get_files(folder_path=self.data_dir) == files_map

    def test_parse_args(self):
        """Test parsing command line arguments."""
        with patch("argparse.ArgumentParser.parse_args") as arg_parse:
            arg_parse.return_value = self.args
            assert parse_args() == self.args

    def test_read_json_file_not_found(self, caplog):
        """Test handling file was not found."""
        json_file = "data.json"
        log_mesg = f"file [{json_file}] not found"

        with caplog.at_level(self.args.log_level, logger="root"):
            assert read_json(json_file) == {}
            assert log_mesg in caplog.text

    def test_read_json_decode_error(self, caplog):
        """Test handling decode error on invalid json data."""
        log_mesg = "invalid json, unable to decode"

        with caplog.at_level(self.args.log_level, logger="root"):
            assert read_json(self.data_files["invalid"]) == {}
            assert log_mesg in caplog.text

    def test_read_json(self):
        """Test reading json file."""
        with open(file=self.data_files["movies"], encoding="utf-8", mode="r") as file:
            json_data = json.load(file)
        assert read_json(self.data_files["movies"]) == json_data

    # fmt: off
    def test_main_missing_schema(self):
        """Test handling missing schema file."""
        with patch("argparse.ArgumentParser.parse_args") as arg_parse, \
             patch(f"{MODULE}.get_files") as files:
            arg_parse.return_value = self.args
            files.return_value = {"schema": "schema.json"}
            with pytest.raises(SystemExit):
                main()
    # fmt: on

    # fmt: off
    def test_main_validation_failed(self, caplog):
        """Test handling failed validation."""
        with patch("argparse.ArgumentParser.parse_args") as arg_parse, \
             patch(f"{MODULE}.get_files") as files:
            arg_parse.return_value = self.args
            schema_file = path.basename(self.data_files["schema"].removesuffix(".json"))
            json_file = schema_file.removesuffix("-schema")
            invalid_file = path.basename(self.data_files["failed"])
            files.return_value = {
                schema_file: self.data_files["schema"],
                json_file: self.data_files["failed"],
            }
            error_mesg = "is a required property"
            info_mesg = " ".join([invalid_file.ljust(40), "FAILED"])

            with caplog.at_level(self.args.log_level, logger="root"):
                with pytest.raises(SystemExit):
                    main()

                assert error_mesg in caplog.text
                assert info_mesg in caplog.text
    # fmt: on

    def test_main(self):
        """Test successful execution of main."""
        with patch("argparse.ArgumentParser.parse_args") as arg_parse:
            arg_parse.return_value = self.args
            main()
